Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: nifti2dicom
Source: https://github.com/biolab-unige/nifti2dicom

Files: *
Copyright: Copyright (C) 2008, 2009, 2010, 2014 Daniele E. Domenichelli <ddomenichelli@drdanz.it>
           Copyright (C) 2010, 2012 Gabriele Arnulfo <gabriele.arnulfo@dist.unige.it>
License: GPL-3+
 Nifti2Dicom is free software: you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation, either version
 3 of the License, or (at your option) any later version.
 .
 Nifti2Dicom is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with Nifti2Dicom.  If not, see
 <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the full text of the GNU General Public
 License version 3 can be found in the file
 `/usr/share/common-licenses/GPL-3'.

Files: src/gui/itkImageToVTKImageFilter.*
Copyright: Copyright (c) 2002 Insight Consortium. All rights reserved.
License: BSD-4-clause-ITK
 Copyright (c) Insight Software Consortium
 All rights reserved.
 .
 Redistribution and use in source and binary forms, with or
 without modification, are permitted provided that the
 following conditions are met:
 .
  * Redistributions of source code must retain the above
    copyright notice, this list of conditions and the
    following disclaimer.
 .
  * Redistributions in binary form must reproduce the above
    copyright notice, this list of conditions and the
    following disclaimer in the documentation and/or other
    materials provided with the distribution.
 .
  * The name of the Insight Consortium, or the names of any
    consortium members, or of any contributors, may not be
    used to endorse or promote products derived from this
    software without specific prior written permission.
 .
  * Modified source versions must be plainly marked as such,
    and must not be misrepresented as being the original
    software.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER AND
 CONTRIBUTORS ``AS IS'' AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 .
 .
 See also the ITK web site: http://www.itk.org for more
 information.

Files: src/gui/vtkKWImage*
Copyright: Copyright (c) Kitware, Inc., Insight Consortium.  All rights reserved.
License: BSD-4-clause-VTK
 Copyright (c) 1993-2002 Ken Martin, Will Schroeder, Bill Lorensen
 All rights reserved.
 .
 Redistribution and use in source and binary forms, with or
 without modification, are permitted provided that the
 following conditions are met:
 .
  * Redistributions of source code must retain the above
    copyright notice, this list of conditions and the
    following disclaimer.
 .
  * Redistributions in binary form must reproduce the above
    copyright notice, this list of conditions and the
    following disclaimer in the documentation and/or other
    materials provided with the distribution.
 .
  * Neither name of Ken Martin, Will Schroeder, or Bill
    Lorensen nor the names of any contributors may be used to
    endorse or promote products derived from this software
    without specific prior written permission.
 .
  * Modified source versions must be plainly marked as such,
    and must not be misrepresented as being the original
    software.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
 CONTRIBUTORS ``AS IS'' AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 .
 .
 See also the VTK web site: http://www.vtk.org for more
 information.


Files: debian/*
Copyright: Copyright (C) Daniele E. Domenichelli <ddomenichelli@drdanz.it>
License: GPL-2+
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later
 version.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.
