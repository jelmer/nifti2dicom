Source: nifti2dicom
Section: science
Priority: optional
Maintainer: Daniele E. Domenichelli <ddomenichelli@drdanz.it>
Build-Depends: debhelper (>= 7.3.16),
 cdbs, cmake (>= 2.6.4), qtbase5-dev | libqt4-dev (>= 4.4),
 libinsighttoolkit4-dev (>= 4.3.0) [i386 amd64] | libinsighttoolkit3-dev,
 libfftw3-dev,
 libtclap-dev (>= 1.2.0),
 libvtk6-qt-dev | libvtk6-dev (<< 6.2) | libvtk5-qt4-dev
Standards-Version: 3.9.7
Homepage: https://github.com/biolab-unige/nifti2dicom
Vcs-Git: https://anonscm.debian.org/git/pkg-exppsy/nifti2dicom.git
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-exppsy/nifti2dicom.git

Package: nifti2dicom-data
Architecture: all
Depends: ${misc:Depends}
Description: data files for nifti2dicom
 This package contains architecture-independent supporting data files
 required for use with nifti2dicom, such as such as documentation, icons,
 and translations.

Package: nifti2dicom
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends},
 nifti2dicom-data (= ${source:Version})
Description: convert 3D medical images to DICOM 2D series
 Nifti2Dicom is a conversion tool that converts 3D NIfTI files (and other
 formats supported by ITK, including Analyze, MetaImage Nrrd and VTK)
 to DICOM.
 Unlike other conversion tools, it can import a DICOM file that is used
 to import the patient and study DICOM tags, and allows you to edit the
 accession number and other DICOM tags, in order to create a valid DICOM
 that can be imported in a PACS.
 .
 This package includes the command line tools.

Package: qnifti2dicom
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends},
 nifti2dicom (= ${binary:Version}), nifti2dicom-data (= ${source:Version})
Description: convert 3D medical images to DICOM 2D series (gui)
 Nifti2Dicom is a conversion tool that converts 3D NIfTI files (and other
 formats supported by ITK, including Analyze, MetaImage Nrrd and VTK)
 to DICOM.
 Unlike other conversion tools, it can import a DICOM file that is used
 to import the patient and study DICOM tags, and allows you to edit the
 accession number and other DICOM tags, in order to create a valid DICOM
 that can be imported in a PACS.
 .
 This package contains the Qt4 GUI.

Package: nifti2dicom-dbg
Section: debug
Priority: extra
Architecture: any
Depends: ${misc:Depends},
 nifti2dicom (= ${binary:Version}) |
 qnifti2dicom (= ${binary:Version})
Description: convert 3D medical images to DICOM 2D series (debug symbols)
 Nifti2Dicom is a conversion tool that converts 3D NIfTI files (and other
 formats supported by ITK, including Analyze, MetaImage Nrrd and VTK)
 to DICOM.
 Unlike other conversion tools, it can import a DICOM file that is used
 to import the patient and study DICOM tags, and allows you to edit the
 accession number and other DICOM tags, in order to create a valid DICOM
 that can be imported in a PACS.
 .
 This package contains the debugging symbols necessary to debug crashes
 in nifti2dicom.
